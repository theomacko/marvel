/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)

// SCSS FILES
require('../scss/style.scss');
require('../scss/admin.scss');
require('../scss/partials/variables.scss');
require('../scss/pages/login.scss');
require('../scss/pages/landingPage.scss');
require('../scss/pages/home.scss');
require('../scss/pages/ficheFilm.scss');
require('../scss/pages/boxOffice.scss');
require('../scss/components/blockFilm.scss');
require('../scss/components/filmPeek.scss');
require('../scss/components/filmsList.scss');
require('../scss/components/header.scss');
require('../scss/components/menu.scss');
require('../scss/components/list-actors.scss');
require('../libs/select2/select2.min.css');
require('../libs/fancybox/jquery.fancybox.min.css');

// FONT FILES
require('../fonts/BentonSans ExtraComp Black.otf');
require('../fonts/BentonSans ExtraComp Bold.otf');
require('../fonts/BentonSans ExtraComp Book.otf');
require('../fonts/BentonSans ExtraComp Light.otf');
require('../fonts/BentonSans ExtraComp Medium.otf');
require('../fonts/BentonSans ExtraComp Regular.otf');

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

// JQUERY PLUGINS
require('select2');
require('jfilestyle');
require('@fancyapps/fancybox')

// JS FILES
require('../js/main');