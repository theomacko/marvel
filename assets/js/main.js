$(window).on('load', function() {

    // region MENU
        function menu() {
            $('.btn-burger').on('click', function(e) {
                e.preventDefault();

                if (!$(this).hasClass('open')) {
                    $(this).addClass('open')
                    $('.menu').addClass('opened')
                    $('body').css('overflow', 'hidden')
                }
            });

            $('.btn-close').on('click', function(e) {
                e.preventDefault();

                if ($('.menu').hasClass('opened')) {
                    $('.menu').removeClass('opened')
                    $('.btn-burger').removeClass('open')
                    $('body').removeAttr('style')
                }
            });
        };
    // endregion MENU

    // region SELECT2
        function select2() {
            var select = $('.form-fields select')

            if (select.length) {
                select.select2();
            }
        };
    // endregion SELECT2

    // region JFILESTYLE
        function fileStyle() {
            var input = $('input:file');

            if (input.length) {
                input.jfilestyle({text: "Choisissez une image"});
            }
        };
    // endregion JFILESTYLE

    // region AJAX
        function ajaxFilm() {
            // region FILM IMAGES
                $('.js-film-images').on('click', function(e) {
                    e.preventDefault();

                    filmImages = $('.js-film');
                    
                    if (!filmImages.hasClass('pop-up')) {
                        filmImages.addClass('pop-up')
                        filmImages.find('input[type="text"]').val('')
                    }
                });

                $('.js-film form').on('submit', function(e){
                    e.preventDefault();

                    urlListFilm = Routing.generate('ajax_list_film_images', {id: $('.js-film-images').data('id')})

                    filmData = new FormData(this);

                    $.ajax({
                        url: urlListFilm,
                        method: "POST",
                        data: filmData,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            $('.js-list-film').html(data)
                            if (filmImages.hasClass('pop-up')) {
                                filmImages.removeClass('pop-up')
                            }
                        },
                        error: function() {
                            console.log('L\'appel ajax a échoué')
                        }
                    })
                })
            // endregion FILM IMAGES

            // region FILM SLIDER
                $('.js-slider-images').on('click', function(e) {
                    e.preventDefault();

                    filmSlider = $('.js-slider')

                    if (!filmSlider.hasClass('pop-up')) {
                        filmSlider.addClass('pop-up')
                        filmSlider.find('input[type="text"]').val('')
                    }
                });

                $('.js-slider form').on('submit', function(e) {
                    e.preventDefault()

                    urlListSlider = Routing.generate('ajax_list_slider_images', {'id': $('.js-slider-images').data('id')});
                    
                    sliderData = new FormData(this)

                    $.ajax({
                        url: urlListSlider,
                        method: 'POST',
                        data: sliderData,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            $('.js-list-slider').html(data)
                            if (filmSlider.hasClass('pop-up')) {
                                filmSlider.removeClass('pop-up')
                            }
                        },
                        error: function() {
                            console.log('L\'appel ajax a échoué')
                        }
                    })
                })
            // endregion FILM SLIDER

            // region CLOSE POP-UP
                $(document).on('click', '.close-pop-up', function(e) {
                    e.preventDefault();

                    if ($(this).parents('.pop-up').length) {
                        $(this).parents('.pop-up').removeClass('pop-up');
                    }
                });
            // endregion POP-UP

            // region DELETE IMAGE CARD
                $(document).on('submit', 'form.delete-image-card', function(e) {
                    e.preventDefault();

                    urlImageCard = Routing.generate('remove_imageCard', {'id': $(this).data('id')});
                    formData = {
                        'image_token' : $(this).find('input[name="_token"]').val()
                    }

                    $.ajax({
                        url: urlImageCard,
                        method: 'POST',
                        data: formData,
                        success: function(data){
                            $('.js-list-film').html(data);
                        },
                        error: function() {
                            console.log('L\'appel ajax a échoué')
                        }
                    });
                });
            // endregion DELETE IMAGE CARD

            // region DELETE IMAGE FICHE
                $(document).on('submit', 'form.delete-image-fiche', function(e) {
                    e.preventDefault();

                    urlImageFiche = Routing.generate('remove_imageFiche', {'id': $(this).data('id')});
                    formData = {
                        'image_token' : $(this).find('input[name="_token"]').val()
                    }

                    $.ajax({
                        url: urlImageFiche,
                        method: 'POST',
                        data: formData,
                        success: function(data){
                            $('.js-list-film').html(data);
                        },
                        error: function() {
                            console.log('L\'appel ajax a échoué')
                        }
                    });
                });
            // endregion DELETE IMAGE FICHE

            // region DELETE IMAGE SLIDER
                $(document).on('submit', 'form.delete-image-slider', function(e) {
                    e.preventDefault();

                    urlImageSlider = Routing.generate('delete_imageSlider', {'id': $(this).data('id')});
                    formData = {
                        'image_token' : $(this).find('input[name="_token"]').val()
                    }

                    $.ajax({
                        url: urlImageSlider,
                        method: 'POST',
                        data: formData,
                        success: function(data){
                            $('.js-list-slider').html(data);
                        },
                        error: function() {
                            console.log('L\'appel ajax a échoué')
                        }
                    });
                });
            // endregion DELETE IMAGE SLIDER
        };

        function ajaxPeek() {
            filmList = $('.js-paginate');
            sneakPeek = $('.js-sneak-peek');

            // region SNEAK PEEK
                $('.js-btn-sneak-peek').each(function() {
                    $(this).on('click', function(e) {
                        e.preventDefault();

                        urlSneakPeek = Routing.generate('film_sneak_peek', {'id': $(this).data('id')});

                        $.ajax({
                            url: urlSneakPeek,
                            method: 'POST',
                            success: function(data) {
                                if (!filmList.hasClass('anim-fade-out')) {
                                    filmList.addClass('anim-fade-out')
                                }
                                
                                setTimeout(function(){
                                    $('body').css('overflow', 'hidden')
                                }, 300)
                                
                                setTimeout(function() {
                                    sneakPeek.html(data)

                                    cardElementDisplay()

                                    if (!sneakPeek.hasClass('anim-fade-in')) {
                                        sneakPeek.addClass('anim-fade-in')
                                    }
                                }, 400)
                            },
                            error: function() {
                                console.log('L\'appel ajax a échoué')
                            }
                        })
                    })
                })
            // endregion SNEAK PEEK

            // region FILM LIST
                $(document).on('click', '.js-close-sneak-peek', function(e) {
                    e.preventDefault();

                    if (sneakPeek.hasClass('anim-fade-in')) {
                        sneakPeek.removeClass('anim-fade-in')
                    }

                    setTimeout(function(){
                        $('body').removeAttr('style')
                    },500)
                    
                    setTimeout(function() {
                        sneakPeek.html('');

                        if (filmList.hasClass('anim-fade-out')) {
                            filmList.removeClass('anim-fade-out')
                        }
                    }, 600)
                })
            // endregion FILM LIST
        }

        function ajaxScroll() {
            $(window).on('scroll', function() {
                target = $('.js-paginate-anchor')
                section = $('.js-paginate')
                url = Routing.generate('films_pagination', {'pageNb': section.data('page-number')})
                offset = section.data('offset')
                dataScroll = {
                    offset
                }

                if ($(document).height() - $(window).height() == $(window).scrollTop()) {
                    if (section.data('films-count') != section.find('.border-blockFilm').length) {
                        target.css('opacity', '0.7')
                        newOffset = offset + section.data('item-per-page')
                        newPageNumber = section.data('page-number') + 1
                        $.ajax({
                            url: url,
                            method: 'POST',
                            data: dataScroll,
                            success: function(data) {
                                section.append($(data).find('.js-paginate').html())
                                section.data('offset', newOffset)
                                section.data('page-number', newPageNumber)
                                target.animate({
                                    opacity: 1 
                                }, 300)
                                ajaxPeek();
                            },
                            error: function() {
                                console.log('L\'appel ajax a échoué')
                            }
                        })
                    }
                }
            })
        };

        function searchFilm() {
            $(document).on('submit', '.search-form', function(e) {
                e.preventDefault();

                type = $(this).data('type');
                searchedItem = $(this).find('input[type="text"]').val();
                formData = {
                    'type' : type
                }
                
                if ($(this).find('input[type="text"]').val().length < 1) {
                    urlSearch = Routing.generate(type + '_search_results', {'searchItem': 'vide'});
                }
                else {
                    urlSearch = Routing.generate(type + '_search_results', {'searchItem': searchedItem});
                }
                
                $.ajax({
                    url: urlSearch,
                    method: 'POST',
                    data: formData,
                    success: function(data) {
                        $('.js-search').html(data);
                    },
                    error: function() {
                        console.log('L\'appel ajax a échoué')
                    }
                });
            })
        };
    // endregion AJAX

    // region HEADER SCROLL
        function headerScroll() {
            header = $('header');

            $(window).on('scroll', function() {
                valueScroll = $(this).scrollTop();
                
                if ($(window).width() > 375) {
                    if (valueScroll >= 95 && !header.hasClass('shadow')) {
                        header.addClass('shadow')
                    }
                    else if (valueScroll < 95 && header.hasClass('shadow')) {
                        header.removeClass('shadow')
                    }
                }
                else {
                    if (valueScroll >= 20 && !header.hasClass('shadow')) {
                        header.addClass('shadow')
                    }
                    else if (valueScroll < 20 && header.hasClass('shadow')) {
                        header.removeClass('shadow')
                    }
                }
            });

            if ($('header').offset().top > 100 && $(window).width() > 375) {
                header.addClass('shadow')
            }
            else if ($('header').offset().top >= 20 && $(window).width() <= 375) {
                header.addClass('shadow')
            }
        };
    // endregion HEADER SCROLL

    // region SHOW/HIDE SNEAK PEEK ELEMENT
        function cardElementDisplay() {
            summary = $('.content-text')
            actions = $('.btn-list')

            $(document).on('click', '.btn-show-summary', function(e) {
                e.preventDefault();

                console.log(summary)

                if (!summary.hasClass('revealed')) {
                    summary.addClass('revealed')
                }
            })

            $(document).on('click', '.btn-hide-summary', function(e) {
                e.preventDefault();

                if (summary.hasClass('revealed')) {
                    summary.removeClass('revealed')
                }
            })

            $(document).on('click', '.btn-show-actions', function(e) {
                e.preventDefault();

                if (!actions.hasClass('revealed')) {
                    actions.addClass('revealed')
                }
            })

            $(document).on('click', '.btn-hide-actions', function(e) {
                e.preventDefault();

                if (actions.hasClass('revealed')) {
                    actions.removeClass('revealed')
                }
            })
        };
    // endregion SHOW/HIDE SNEAK PEEK ELEMENT

    // region FICHE FILM
        function animElmts() {
            if (filmContainer.length > 0) {
                if (filmContainer.offset().top - $(this).scrollTop() <= ($(this).height() / 2)) {
                    $('.js-animate').each(function() {
                        $(this).addClass($(this).data('anim'))
                    })   
                }
            }

            if (listContainer.length > 0) {
                listContainer.each(function() {
                    if ($(this).offset().top - $(window).scrollTop() <= $(window).height() / 2) {
                        i = 0;
                        $(this).find('.js-small-animate').each(function() {
                            if (i == 0) {
                                $(this).addClass($(this).data('anim'))
                                i = i + 0.3;
                            }
                            else {
                                $(this).css('animation-delay', i + 's')
                                $(this).addClass($(this).data('anim'))  
                                i = i + 0.3;
                            }
                        })
                    }
                })
            }

            if (relatedFilm.length > 0) {
                relatedFilm.each(function() {
                    if ($(this).offset().top - $(window).scrollTop() <= $(window).height() / 2) {
                        $(this).addClass($(this).data('anim'))
                    }
                })
            }
        };
    
        function animFilm() {
            filmContainer = $('.content-film')
            listContainer = $('.js-list-animate')
            relatedFilm = $('.js-related-animate')

            $(window).on('scroll', function() {
                animElmts();
            })
        };

        function slider() {
            $('.slider img').first().addClass('active')
            $('.slider-nav a').first().addClass('active')
            $('.slider').css('transform', 'translateX(0px)')

            $('.slider-nav a').each(function() {
                $(this).on('click', function(e) {
                    e.preventDefault();

                    if (!$(this).hasClass('active')) {
                        indexDif = $(this).data('index') - $('.slider-nav a.active').data('index')
                        $('.slider-nav a').removeClass('active')
                        $(this).addClass('active')
                        
                        $('.slider img').each(function() {
                            if ($(this).data('index') == $('.slider-nav a.active').data('index')) {
                                slideValue = parseInt($('.slider').css('transform').split(',')[4]) - $('.film-slider').width() * indexDif
                                $('.slider').css('transform', 'translateX(' + slideValue +'px)')

                                $('.slider img').removeClass('active')
                                $(this).addClass('active')
                            }
                        })
                    }
                })
            })
        };
    // endregion FICHE FILM

    // region INIT FANCYBOX
        $('.fancybox').fancybox({
            'loop': true
        })
    // endregion INIT FANCYBOX

    // region INIT
        menu();
        select2();
        fileStyle();
        ajaxFilm();
        ajaxPeek();
        ajaxScroll();
        searchFilm();
        headerScroll();
        animFilm();
        animElmts();
        slider();
    // endregion INIT
});