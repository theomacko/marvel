<?php

namespace App\Controller;

use App\Repository\ActorsRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ActorsController extends AbstractController
{
    private $actorsRepository;

    public function __construct(ActorsRepository $actorsRepository)
    {
        $this->actorsRepository = $actorsRepository;
    }

    /**
     * @Route("/actors", name="actors_home")
     */
    public function homeActors()
    {
        $actors = $this->actorsRepository->findAll();

        return $this->render('pages/actors.html.twig', [
            'actors' => $actors
        ]);
    }
}
