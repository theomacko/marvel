<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\FilmRepository;

class BoxOfficeController extends AbstractController
{
    private $filmRepository;

    public function __construct(FilmRepository $filmRepository)
    {
        $this->filmRepository = $filmRepository;
    }

    /**
     * @Route("/box-office", name="home_box_office")
     */
    public function homeBoxOffice()
    {
        $boxOfficeFilms = $this->filmRepository->filmByBoxOffice();

        return $this->render('pages/box_office.html.twig', [
            'films' => $boxOfficeFilms
        ]);
    }
}
