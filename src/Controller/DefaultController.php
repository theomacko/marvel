<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    private $filmRepository;
    private $offset = 0;
    private $itemNb = 6;

    public function __construct(FilmRepository $filmRepository)
    {
        $this->filmRepository = $filmRepository;
    }

    /**
     * @Route("/", name="landing_page")
     */
    public function landing()
    {
        return $this->render('default/landing.html.twig');
    }

    /**
     * @Route("/home", name="home_page")
     */
    public function home()
    {
        $limit = $this->itemNb;
        $offset = $this->offset;
        $films = $this->filmRepository->findByPageNumber($offset, $limit);

        $filmsCount = count($this->filmRepository->findAll());

        return $this->render('pages/home.html.twig', [
            'films' => $films,
            'filmsCount' => $filmsCount
        ]);
    }

    /**
     * @Route("/home/films/page={pageNb}", name="films_pagination", options={"expose"=true})
     */
    public function ajaxPagination(Request $request, $pageNb)
    {
        if ($request->isXmlHttpRequest()) {
            $offset = $request->request->get('offset');
            $offset = $offset;

            $limit = $this->itemNb;
            $films = $this->filmRepository->findByPageNumber($offset, $limit);

            $filmsCount = count($this->filmRepository->findAll());

            return $this->render('components/preview.html.twig', [
                'pageNb' => (int)$pageNb + 1,
                'offset' => (int)$offset + $this->itemNb,
                'films' => $films,
                'filmsCount' => $filmsCount
            ]);
        }
    }
}
