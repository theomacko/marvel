<?php

namespace App\Controller;

use App\Repository\DirectorsRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DirectorsController extends AbstractController
{
    private $directorsRepository;

    public function __construct(DirectorsRepository $directorsRepository)
    {
        $this->directorsRepository = $directorsRepository;
    }

    /**
     * @Route("/directors", name="directors_home")
     */
    public function homeDirectors()
    {
        $directors = $this->directorsRepository->findAll();

        return $this->render('pages/directors.html.twig', [
            'directors' => $directors
        ]);
    }
}
