<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ImageSliderRepository;
use App\Services\QueryService;

class FilmController extends AbstractController
{
    private $filmRepository;
    private $sliderRepository;
    private $queryService;

    public function __construct(FilmRepository $filmRepository, ImageSliderRepository $sliderRepository, QueryService $queryService)
    {
        $this->filmRepository = $filmRepository;
        $this->sliderRepository = $sliderRepository;
        $this->queryService = $queryService;
    }

    /**
     * @Route("/home/sneakPeek/film/{id}", name="film_sneak_peek", options={"expose"=true})
     */
    public function sneakPeek(Request $request, $id)
    {
        if ($request->isXmlHttpRequest()) {
            $film = $this->filmRepository->find($id);

            return $this->render('components/Ajax/_AjaxSneakPeek.html.twig', [
                'film' => $film
            ]);
        }
    }

    /**
     * @Route("/films/{id}", name="fiche_film")
     */
    public function ficheFilm($id)
    {
        $film = $this->filmRepository->find($id);
        $sliderImages = $this->sliderRepository->findByFilmId($id);
        $allMovies = $this->filmRepository->findAll();

        $relatedFilms = $this->queryService->getRelatedFilms($film, $allMovies);

        return $this->render('pages/ficheFilm.html.twig', [
            'film' => $film,
            'sliderImages' => $sliderImages,
            'related_films' => $relatedFilms
        ]);
    }
}
