<?php

namespace App\Controller\admin;

use App\Entity\Actors;
use App\Form\ActorsType;
use App\Repository\ActorsRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Services\QueryService;

class AdminActorsController extends AbstractController
{   
    private $actorsRepository;
    private $om;
    private $queryService;

    public function __construct(ActorsRepository $actorsRepository, ObjectManager $om, QueryService $queryService)
    {
        $this->actorsRepository = $actorsRepository;
        $this->om = $om;
        $this->queryService = $queryService;
    }

    /**
     * @Route("/admin/actors", name="home_actors")
     */
    public function homeActors()
    {
        $actors = $this->actorsRepository->findAll();

        return $this->render('admin/actors/homeActors.html.twig', [
            'actors' => $actors
        ]);
    }

    /**
     * @Route("/admin/actors/search/{searchItem}", name="actor_search_results", options={"expose"=true})
     */
    public function actorSearch(Request $request, $searchItem)
    {
        if ($request->isXmlHttpRequest()) {
            $repository = $this->actorsRepository;

            $results = $this->queryService->getSearchResults($request, $searchItem, $repository);

            return $this->render('admin/actors/Ajax/_actorSearchResult.html.twig', [
                'actors' => $results['result'],
                'searchItem' => $results['searchItem'],
                'searchResult' => $results['searchResult'],
            ]);
        }
    }

    /**
     * @Route("/admin/actors/new", name="new_actor")
     */
    public function newActor(Request $request)
    {
        $actor = new Actors();

        $formActor = $this->createForm(ActorsType::class, $actor);
        $formActor->handleRequest($request);

        $error = null;

        if ($formActor->isSubmitted() && $formActor->isValid()) {
            $currentActor = $this->actorsRepository->findByName($request->request->get('actors')['name']);

            if (count($currentActor) <= 0) {
                $this->om->persist($actor);
                $this->om->flush();

                return $this->redirectToRoute('home_actors');
            } else {
                $error = 'Cet acteur existe déjà';
            }
        }

        return $this->render('admin/actors/newActor.html.twig', [
            'error' => $error,
            'formActor' => $formActor->createView(),
            'actor' => $actor
        ]);
    }

    /**
     * @Route("/admin/actors/edit/{id}", name="edit_actor", methods="GET|POST")
     */
    public function editActor(Request $request, $id)
    {
        $actor = $this->actorsRepository->find($id);

        $formActor = $this->createForm(ActorsType::class, $actor);
        $formActor->handleRequest($request);

        if ($formActor->isSubmitted() && $formActor->isValid()) {
            $this->om->flush();

            $this->addFlash('success', 'L\'acteur a bien été modifé');

            return $this->redirectToRoute('edit_actor', [
                'id' => $id
            ]);
        }

        return $this->render('admin/actors/editActor.html.twig', [
            'formActor' => $formActor->createView(),
            'actor' => $actor
        ]);
    }

    /**
     * @Route("/admin/actors/delete/{id}", name="delete_actor", methods="DELETE")
     */
    public function deleteActor(Request $request, $id)
    {
        $actor = $this->actorsRepository->find($id);

        if ($this->isCsrfTokenValid('delete' . $actor->getId(), $request->get('_token'))) {
            $this->om->persist($actor);
            $this->om->remove($actor);
            $this->om->flush();

            return $this->redirectToRoute('home_actors');
        }
    }
}
