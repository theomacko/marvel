<?php

namespace App\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\FilmRepository;

class AdminDefaultController extends AbstractController
{
    private $filmRepository;

    public function __construct(FilmRepository $filmRepository)
    {
        $this->filmRepository = $filmRepository;
    }

    /**
     * @Route("/admin", name="admin_home")
     */
    public function adminHome()
    {
        return $this->render('admin/adminHome.html.twig', [
            
        ]);
    }
}
