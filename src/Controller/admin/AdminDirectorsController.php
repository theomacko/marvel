<?php

namespace App\Controller\admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\DirectorsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Directors;
use App\Form\DirectorsType;
use App\Services\QueryService;

class AdminDirectorsController extends AbstractController
{   
    private $directorsRepository;
    private $om;
    private $queryService;

    public function __construct(DirectorsRepository $directorsRepository, ObjectManager $om, QueryService $queryService)
    {
        $this->directorsRepository = $directorsRepository;
        $this->om = $om;
        $this->queryService = $queryService;
    }

    /**
     * @Route("/admin/directors", name="home_directors")
     */
    public function homeDirector()
    {
        $directors = $this->directorsRepository->findAll();

        return $this->render('admin/directors/homeDirectors.html.twig', [
            'directors' => $directors
        ]);
    }

    /**
     * @Route("/admin/directors/search/{searchItem}", name="director_search_results", options={"expose"=true})
     */
    public function directorSearch(Request $request, $searchItem)
    {
        $repository = $this->directorsRepository;

        $results = $this->queryService->getSearchResults($request, $searchItem, $repository);

        return $this->render('admin/directors/Ajax/_directorSearchResult.html.twig', [
            'directors' => $results['result'],
            'searchItem' => $results['searchItem'],
            'searchResult' => $results['searchResult']
        ]);
    }

    /**
     * @Route("/admin/directors/new", name="new_director")
     */
    public function newDirector(Request $request)
    {
        $director = new Directors();

        $formDirector = $this->createForm(DirectorsType::class, $director);
        $formDirector->handleRequest($request);

        $error = null;

        if ($formDirector->isSubmitted() && $formDirector->isValid()) {
            $currentDirector = $this->directorsRepository->findByName($request->request->get('directors')['name']);
            
            if (count($currentDirector) <= 0) {
                $this->om->persist($director);
                $this->om->flush();

                return $this->redirectToRoute('home_directors');
            } else {
                $error = 'Ce réalisateur existe déjà';
            }
        }

        return $this->render('admin/directors/newDirector.html.twig', [
            'error' => $error,
            'formDirector' => $formDirector->createView()
        ]);
    }

    /**
     * @Route("/admin/directors/edit/{id}", name="edit_director", methods="GET|POST")
     */
    public function editDirector(Request $request, $id)
    {
        $director = $this->directorsRepository->find($id);

        $formDirector = $this->createForm(DirectorsType::class, $director);
        $formDirector->handleRequest($request);

        if ($formDirector->isSubmitted() && $formDirector->isValid()) {
            $this->om->flush();

            $this->addFlash('success', 'Le réalisateur a bien été modifé');

            return $this->redirectToRoute('edit_director', [
                'id' => $id
            ]);
        }

        return $this->render('admin/directors/editDirector.html.twig', [
            'formDirector' => $formDirector->createView(),
            'director' => $director
        ]);
    }

    /**
     * @Route("/admin/directors/delete/{id}", name="delete_director", methods="DELETE")
     */
    public function deleteDirector(Request $request, $id)
    {
        $director = $this->directorsRepository->find($id);

        if ($this->isCsrfTokenValid('delete' . $director->getId(), $request->get('_token'))) {
            $this->om->persist($director);
            $this->om->remove($director);
            $this->om->flush();

            return $this->redirectToRoute('home_directors');
        }
    }
}
