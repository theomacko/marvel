<?php

namespace App\Controller\admin;

use App\Entity\Film;
use App\Form\NewFilmType;
use App\Repository\FilmRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\FilmType;
use App\Form\ImageSliderType;
use App\Entity\ImageSlider;
use App\Repository\ImageSliderRepository;
use App\Form\FilmImagesType;
use App\Services\QueryService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdminFilmController extends AbstractController
{
    private $filmRepository;
    private $imageSliderRepository;
    private $om;
    private $queryService;
    private $params;

    public function __construct(FilmRepository $filmRepository, ImageSliderRepository $imageSliderRepository,
    ObjectManager $om, QueryService $queryService, ParameterBagInterface $params)
    {
        $this->filmRepository = $filmRepository;
        $this->imageSliderRepository = $imageSliderRepository;
        $this->om = $om;
        $this->queryService = $queryService;
        $this->params = $params;
    }

    /**
     * @Route("/admin/films", name="home_films")
     */
    public function homeFilms()
    {
        $films = $this->filmRepository->findAll();

        return $this->render('admin/films/homeFilms.html.twig', [
            'films' => $films
        ]);
    }

    /**
     * @Route("/admin/films/search/{searchItem}", name="film_search_results", options={"expose"=true})
     */
    public function filmSearch(Request $request, $searchItem)
    {
        if ($request->isXmlHttpRequest()) {
            $repository = $this->filmRepository;

            $results = $this->queryService->getSearchResults($request, $searchItem, $repository);

            return $this->render('admin/films/Ajax/_filmSearchResult.html.twig', [
                'films' => $results['result'],
                'searchItem' => $results['searchItem'],
                'searchResult' => $results['searchResult']
            ]);
        }
    }

    /**
     * @Route("/admin/films/new", name="new_film")
     */
    public function newFilm(Request $request)
    {
        $film = new Film();
        
        $newFormFilm = $this->createForm(NewFilmType::class, $film);
        $newFormFilm->handleRequest($request);

        $error = null;
        
        if ($newFormFilm->isSubmitted() && $newFormFilm->isValid()) {
            $currentFilm = $this->filmRepository->findByTitle($request->request->get('new_film')['title']);

            if (count($currentFilm) <= 0) {
                $this->om->persist($film);
                $this->om->flush();

                return $this->redirectToRoute('home_films');
            } else {
                $error = 'Ce film existe déjà';
            }
        }

        return $this->render('admin/films/newFilm.html.twig', [
            'error' => $error,
            'newFormFilm' => $newFormFilm->createView()
        ]);
    }

    /**
     * @Route("/admin/films/edit/{id}", name="edit_film", methods="GET|POST")
     */
    public function editFilm(Request $request, $id)
    {
        $film = $this->filmRepository->find($id);
        $newImageSlider = new ImageSlider();
        $imagesSlider = $this->imageSliderRepository->findByFilmId($id);  
        $formSlider =$this->createForm(ImageSliderType::class, $newImageSlider);        

        $formFilm = $this->createForm(FilmType::class, $film);
        $formFilm->handleRequest($request);

        $formImages = $this->createForm(FilmImagesType::class, $film);

        $error = null;

        if ($formFilm->isSubmitted() && $formFilm->isValid()) {
            $this->om->flush();

            $this->addFlash('success', 'Le film a bien été modifié');

            return $this->redirectToRoute('edit_film', [
                'id' => $id
            ]);
        }
        
        return $this->render('admin/films/editFilm.html.twig', [
            'formFilm' => $formFilm->createView(),
            'film' => $film,
            'imagesSlider' => $imagesSlider,
            'error' => $error,
            'formImages' => $formImages->createView(),
            'formSlider' => $formSlider->createView()
        ]);
    }

    /**
     * @Route("/admin/films/remove-image-card/{id}", name="remove_imageCard", options={"expose"=true})
     */
    public function removeImageCard(Request $request, $id)
    {
        $film = $this->filmRepository->find($id);

        if ($request->isXmlHttpRequest() && $this->isCsrfTokenValid('remove' . $film->getId(), $request->request->get('image_token'))) {
            $film->setImageCardName(null);
            $this->om->flush();

            $film = $this->filmRepository->find($id);

            return $this->render('admin/films/Ajax/_listFilmImages.html.twig', [
                'film' => $film
            ]);
        }
    }

    /**
     * @Route("/admin/films/remove-image-fiche/{id}", name="remove_imageFiche", options={"expose"=true})
     */
    public function removeImageFiche(Request $request, $id)
    {
        $film = $this->filmRepository->find($id);

        if ($request->isXmlHttpRequest() && $this->isCsrfTokenValid('remove' . $film->getId(), $request->request->get('image_token'))) {
            $film->setImageFicheName(null);
            $this->om->flush();

            $film = $this->filmRepository->find($id);

            return $this->render('admin/films/Ajax/_listFilmImages.html.twig', [
                'film' => $film
            ]);
        }
    }

    /**
     * @Route("/admin/films/delete-image-slider/{id}", name="delete_imageSlider", options={"expose"=true})
     */
    public function deleteImageSlider(Request $request, $id)
    {
        $imageSlider = $this->imageSliderRepository->find($id);
        $filmId = $imageSlider->getFilmId();

        if ($request->isXmlHttpRequest() && $this->isCsrfTokenValid('delete' . $imageSlider->getId(), $request->request->get('image_token'))) {
            $this->om->remove($imageSlider);
            $this->om->flush(); 

            $imagesSlider = $this->imageSliderRepository->findByFilmId($filmId); 
            $error = null;

            return $this->render('admin/films/Ajax/_listSliderImages.html.twig', [
                'imagesSlider' => $imagesSlider,
                'error' => $error
            ]);
        }
    }

    /**
     * @Route("/admin/films/delete/{id}", name="delete_film", methods="DELETE")
     */
    public function deleteFilm(Request $request, $id)
    {
        $film = $this->filmRepository->find($id);

        if ($this->isCsrfTokenValid('delete' . $film->getId(), $request->get('_token'))) {
            $this->om->persist($film);
            $this->om->remove($film);
            $this->om->flush();

            return $this->redirectToRoute('home_films');
        }
    }


    // AJAX EDIT FILM SLIDER

    /**
     * @Route("/admin/films/edit/{id}/list-slider", name="ajax_list_slider_images", options={"expose"=true})
     */
    public function listSliderImages(Request $request, $id)
    {
        $imagesSlider = $this->imageSliderRepository->findByFilmId($id);  
        $error = null;
        
        if ($request->isXmlHttpRequest() && count($imagesSlider) < 3) {            
            if (!empty($request->files->get('image_slider')['imageSliderFile'])) {
                $newImageSlider = new ImageSlider();

                $sliderName = $request->files->get('image_slider')['imageSliderFile']->getClientOriginalName();
                $sliderPath = $request->files->get('image_slider')['imageSliderFile']->getPathName();
                $sliderTarget = $this->params->get('upload.directory') . '/images/films/slider/' . $sliderName;
                $sliderTarget = str_replace('\\', '/', $sliderTarget);
                move_uploaded_file($sliderPath, $sliderTarget);

                $this->om->persist($newImageSlider);
                $newImageSlider->setImageSliderName($sliderName);
                $newImageSlider->setFilmId($id);
                $newImageSlider->setUpdatedAt(new \DateTime('now'));
                
                $this->om->flush();
            }
        }
        else {
            $error = 'Vous avez atteint le nombre maximum d\'images';
        }

        $imagesSlider = $this->imageSliderRepository->findByFilmId($id); 

        return $this->render('admin/films/Ajax/_listSliderImages.html.twig', [
            'imagesSlider' => $imagesSlider,
            'error' => $error
        ]);
    }


    // AJAX EDIT FILM IMAGES

    /**
     * @Route("/admin/films/edit/{id}/list-images", name="ajax_list_film_images", options={"expose"=true})
     */
    public function listFilmImages(Request $request, $id) {
        $film = $this->filmRepository->find($id);

        if ($request->isXmlHttpRequest()) {
            if (!empty($request->files->get('film_images')['imageCardFile']) ||
            !empty($request->files->get('film_images')['imageFicheFile'])) {             

                if (!empty($request->files->get('film_images')['imageCardFile'])) {
                    $cardName = $request->files->get('film_images')['imageCardFile']->getClientOriginalName();
                    $urlCardImage = $request->files->get('film_images')['imageCardFile']->getPathName();
                    $cardTarget = $this->params->get('upload.directory') . '/images/films/cards/' . $cardName;
                    $cardTarget = str_replace('\\', '/', $cardTarget);
                    move_uploaded_file($urlCardImage, $cardTarget);

                    $film->setImageCardName($cardName);
                }
                
                if (!empty($request->files->get('film_images')['imageFicheFile'])) {
                    $ficheName = $request->files->get('film_images')['imageFicheFile']->getClientOriginalName();
                    $urlFicheImage = $request->files->get('film_images')['imageFicheFile']->getPathName();
                    $ficheTarget = $this->params->get('upload.directory') . '/images/films/fiches/' . $ficheName;
                    $ficheTarget = str_replace('\\', '/', $ficheTarget);
                    move_uploaded_file($urlFicheImage, $ficheTarget);

                    $film->setImageFicheName($ficheName);
                }
                
                $this->om->flush();
            }
        }

        $film = $this->filmRepository->find($id);

        return $this->render('admin/films/Ajax/_listFilmImages.html.twig', [
            'film' => $film
        ]);
    }
}
