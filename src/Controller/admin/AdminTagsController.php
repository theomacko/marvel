<?php

namespace App\Controller\admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TagsRepository;
use App\Form\TagsType;
use App\Entity\Tags;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Services\QueryService;

class AdminTagsController extends AbstractController
{
    private $tagsRepository;
    private $om;
    private $queryService;

    public function __construct(TagsRepository $tagsRepository, ObjectManager $om, QueryService $queryService)
    {
        $this->tagsRepository = $tagsRepository;
        $this->om = $om;
        $this->queryService = $queryService;
    }

    /**
     * @Route("/admin/tags", name="home_tags")
     */
    public function homeTags()
    {
        $tags = $this->tagsRepository->findAll();

        return $this->render('admin/tags/homeTags.html.twig', [
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/admin/tags/search/{searchItem}", name="tag_search_results", options={"expose"=true})
     */
    public function tagSearch(Request $request, $searchItem)
    {
        $repository = $this->tagsRepository;

        $results = $this->queryService->getSearchResults($request, $searchItem, $repository);

        return $this->render('admin/tags/Ajax/_tagSearchResult.html.twig', [
            'tags' => $results['result'],
            'searchItem' => $results['searchItem'],
            'searchResult' => $results['searchResult']
        ]);
    }

    /**
     * @Route("/admin/tags/new", name="new_tag")
     */
    public function newTag(Request $request)
    {
        $tag = new Tags();

        $formTag = $this->createForm(TagsType::class, $tag);
        $formTag->handleRequest($request);

        $error = null;

        if ($formTag->isSubmitted() && $formTag->isValid()) {
            $currentTag = $this->tagsRepository->findByName($request->request->get('tags')['name']);

            if (count($currentTag) <= 0) {
                $this->om->persist($tag);
                $this->om->flush();

                return $this->redirectToRoute('home_tags');
            } else {
                $error = 'Ce tag existe déjà';
            }
        }

        return $this->render('admin/tags/newTag.html.twig', [
            'error' => $error,
            'formTag' => $formTag->createView()
        ]);
    }

    /**
     * @Route("/admin/tags/edit/{id}", name="edit_tag", methods="GET|POST")
     */
    public function editTag(Request $request, $id)
    {
        $tag = $this->tagsRepository->find($id);

        $formTag = $this->createForm(TagsType::class, $tag);
        $formTag->handleRequest($request);

        if ($formTag->isSubmitted() && $formTag->isValid()) {
            $this->om->flush();

            $this->addFlash('success', 'Le tag a bien été modifié');

            return $this->redirectToRoute('edit_tag', [
                'id' => $id
            ]);
        }

        return $this->render('admin/tags/editTag.html.twig', [
            'formTag' => $formTag->createView()
        ]);
    }

    /**
     * @Route("/admin/tags/delete/{id}", name="delete_tag", methods="DELETE")
     */
    public function deleteTag(Request $request, $id)
    {
        $tag = $this->tagsRepository->find($id);

        if ($this->isCsrfTokenValid('delete' . $tag->getId(), $request->get('_token'))) {
            $this->om->persist($tag);
            $this->om->remove($tag);
            $this->om->flush();

            return $this->redirectToRoute('home_tags');
        }
    }
}
