<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $pwdEncoder;

    public function __construct(UserPasswordEncoderInterface $pwdEncoder)
    {
        $this->pwdEncoder = $pwdEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();

        $user  
            ->setUsername('admin')
            ->setPassword($this->pwdEncoder->encodePassword($user, ('admin')))
        ;

        $manager->persist($user);
        $manager->flush();
    }
}
