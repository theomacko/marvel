<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\ActorsRepository")
 */
class Actors
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caracter;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageFileName;

    /**
     * @var File|null
     * @Assert\Image(mimeTypes={"image/png", "image/jpeg"})
     * @Vich\UploadableField(mapping="actor_fiches", fileNameProperty="imageFileName")
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Film", mappedBy="movieActors")
     */
    private $movieActed;

    public function __construct()
    {
        $this->movieActed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCaracter(): ?string
    {
        return $this->caracter;
    }

    public function setCaracter(string $caracter): self
    {
        $this->caracter = $caracter;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * Get the value of imageFile
     *
     * @return  File
     */ 
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set the value of imageFile
     *
     * @param  File  $imageFile
     *
     * @return  self
     */ 
    public function setImageFile(File $imageFile)
    {
        $this->imageFile = $imageFile;

        if ($this->imageFile instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return Collection|Film[]
     */
    public function getMovieActed(): Collection
    {
        return $this->movieActed;
    }

    public function addMovieActed(Film $movieActed): self
    {
        if (!$this->movieActed->contains($movieActed)) {
            $this->movieActed[] = $movieActed;
            $movieActed->addMovieActor($this);
        }

        return $this;
    }

    public function removeMovieActed(Film $movieActed): self
    {
        if ($this->movieActed->contains($movieActed)) {
            $this->movieActed->removeElement($movieActed);
            $movieActed->removeMovieActor($this);
        }

        return $this;
    }
}
