<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="App\Repository\DirectorsRepository")
 */
class Directors
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageFileName;

    /**
     * @var File|null
     * @Assert\Image(mimeTypes={"image/png", "image/jpeg"}) 
     * @Vich\UploadableField(mapping="director_fiches", fileNameProperty="imageFileName")
     */
    private $imageFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Film", mappedBy="movieDirectors")
     */
    private $movieDirected;

    public function __construct()
    {
        $this->movieDirected = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * @return Collection|Film[]
     */
    public function getMovieDirected(): Collection
    {
        return $this->movieDirected;
    }

    public function addMovieDirected(Film $movieDirected): self
    {
        if (!$this->movieDirected->contains($movieDirected)) {
            $this->movieDirected[] = $movieDirected;
            $movieDirected->addMovieDirector($this);
        }

        return $this;
    }

    public function removeMovieDirected(Film $movieDirected): self
    {
        if ($this->movieDirected->contains($movieDirected)) {
            $this->movieDirected->removeElement($movieDirected);
            $movieDirected->removeMovieDirector($this);
        }

        return $this;
    }

    /**
     * Get the value of imageFile
     *
     * @return  File|null
     */ 
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set the value of imageFile
     *
     * @param  File|null  $imageFile
     *
     * @return  self
     */ 
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        if ($this->imageFile instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }
}
