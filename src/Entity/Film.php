<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FilmRepository")
 */
class Film
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $boxOffice;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageCardName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageFicheName;

    /**
     * @var File|null
     */
    private $imageCardFile;

    /**
     * @var File|null
     */
    private $imageFicheFile;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", inversedBy="tagFilms")
     */
    private $movieTag;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Actors", inversedBy="movieActed")
     */
    private $movieActors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Directors", inversedBy="movieDirected")
     */
    private $movieDirectors;

    public function __construct()
    {
        $this->movieTag = new ArrayCollection();
        $this->created_at = new \DateTime();
        $this->movieActors = new ArrayCollection();
        $this->movieDirectors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getBoxOffice(): ?float
    {
        return $this->boxOffice;
    }

    public function setBoxOffice(float $boxOffice): self
    {
        $this->boxOffice = $boxOffice;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getImageCardName(): ?string
    {
        return $this->imageCardName;
    }

    public function setImageCardName(?string $imageCardName): self
    {
        $this->imageCardName = $imageCardName;

        return $this;
    }

    public function getImageFicheName(): ?string
    {
        return $this->imageFicheName;
    }

    public function setImageFicheName(?string $imageFicheName): self
    {
        $this->imageFicheName = $imageFicheName;

        return $this;
    }

    /**
     * Get the value of imageCardFile
     *
     * @return  File|null
     */ 
    public function getImageCardFile()
    {
        return $this->imageCardFile;
    }

    /**
     * Set the value of imageCardFile
     *
     * @param  File|null  $imageCardFile
     *
     * @return  self
     */ 
    public function setImageCardFile($imageCardFile)
    {
        $this->imageCardFile = $imageCardFile;

        return $this;
    }

    /**
     * Get the value of imageFicheFile
     *
     * @return  File|null
     */ 
    public function getImageFicheFile()
    {
        return $this->imageFicheFile;
    }

    /**
     * Set the value of imageFicheFile
     *
     * @param  File|null  $imageFicheFile
     *
     * @return  self
     */ 
    public function setImageFicheFile($imageFicheFile)
    {
        $this->imageFicheFile = $imageFicheFile;

        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getMovieTag(): Collection
    {
        return $this->movieTag;
    }

    public function addMovieTag(Tags $movieTag): self
    {
        if (!$this->movieTag->contains($movieTag)) {
            $this->movieTag[] = $movieTag;
        }

        return $this;
    }

    public function removeMovieTag(Tags $movieTag): self
    {
        if ($this->movieTag->contains($movieTag)) {
            $this->movieTag->removeElement($movieTag);
        }

        return $this;
    }

    /**
     * @return Collection|Actors[]
     */
    public function getMovieActors(): Collection
    {
        return $this->movieActors;
    }

    public function addMovieActor(Actors $movieActor): self
    {
        if (!$this->movieActors->contains($movieActor)) {
            $this->movieActors[] = $movieActor;
        }

        return $this;
    }

    public function removeMovieActor(Actors $movieActor): self
    {
        if ($this->movieActors->contains($movieActor)) {
            $this->movieActors->removeElement($movieActor);
        }

        return $this;
    }

    /**
     * @return Collection|Directors[]
     */
    public function getMovieDirectors(): Collection
    {
        return $this->movieDirectors;
    }

    public function addMovieDirector(Directors $movieDirector): self
    {
        if (!$this->movieDirectors->contains($movieDirector)) {
            $this->movieDirectors[] = $movieDirector;
        }

        return $this;
    }

    public function removeMovieDirector(Directors $movieDirector): self
    {
        if ($this->movieDirectors->contains($movieDirector)) {
            $this->movieDirectors->removeElement($movieDirector);
        }

        return $this;
    }
}
