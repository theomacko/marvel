<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageSliderRepository")
 */
class ImageSlider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageSliderName;

    /**
     * @var File|null
     */
    private $imageSliderFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $filmId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageSliderName(): ?string
    {
        return $this->imageSliderName;
    }

    public function setImageSliderName(?string $imageSliderName): self
    {
        $this->imageSliderName = $imageSliderName;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of imageSliderFile
     *
     * @return  File
     */ 
    public function getImageSliderFile()
    {
        return $this->imageSliderFile;
    }

    /**
     * Set the value of imageSliderFile
     *
     * @param  File  $imageSliderFile
     *
     * @return  self
     */ 
    public function setImageSliderFile(File $imageSliderFile)
    {
        $this->imageSliderFile = $imageSliderFile;

        return $this;
    }

    public function getFilmId(): ?int
    {
        return $this->filmId;
    }

    public function setFilmId(?int $filmId): self
    {
        $this->filmId = $filmId;

        return $this;
    }
}
