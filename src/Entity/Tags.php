<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TagsRepository")
 */
class Tags
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Film", mappedBy="movieTag")
     */
    private $tagFilms;

    public function __construct()
    {
        $this->tagFilms = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Film[]
     */
    public function getTagFilms(): Collection
    {
        return $this->tagFilms;
    }

    public function addTagFilm(Film $tagFilm): self
    {
        if (!$this->tagFilms->contains($tagFilm)) {
            $this->tagFilms[] = $tagFilm;
            $tagFilm->addMovieTag($this);
        }

        return $this;
    }

    public function removeTagFilm(Film $tagFilm): self
    {
        if ($this->tagFilms->contains($tagFilm)) {
            $this->tagFilms->removeElement($tagFilm);
            $tagFilm->removeMovieTag($this);
        }

        return $this;
    }
}
