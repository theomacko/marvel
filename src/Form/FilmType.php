<?php

namespace App\Form;

use App\Entity\Film;
use App\Entity\Tags;
use App\Entity\Actors;
use App\Entity\Directors;
use App\Entity\ImageSlider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('year')
            ->add('duration')
            ->add('boxOffice')
            ->add('summary')
            ->add('movieTag', EntityType::class, [
                'multiple' => true,
                'required' => false,
                'class' => Tags::class,
                'choice_label' => 'name'
            ])
            ->add('movieActors', EntityType::class, [
                'class' => Actors::class,
                'required' => false,
                'choice_label' => 'name',
                'multiple' => true
            ])
            ->add('movieDirectors', EntityType::class, [
                'class' => Directors::class,
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
        ]);
    }
}
