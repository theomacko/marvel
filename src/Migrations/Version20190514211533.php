<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190514211533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film CHANGE title title VARCHAR(255) DEFAULT NULL, CHANGE year year INT DEFAULT NULL, CHANGE duration duration INT DEFAULT NULL, CHANGE box_office box_office INT DEFAULT NULL, CHANGE summary summary LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film CHANGE title title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE year year INT NOT NULL, CHANGE duration duration INT NOT NULL, CHANGE box_office box_office INT NOT NULL, CHANGE summary summary LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
