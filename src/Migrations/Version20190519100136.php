<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519100136 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE actors_tags (actors_id INT NOT NULL, tags_id INT NOT NULL, INDEX IDX_6673CAE47168CF59 (actors_id), INDEX IDX_6673CAE48D7B4FB4 (tags_id), PRIMARY KEY(actors_id, tags_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE directors_tags (directors_id INT NOT NULL, tags_id INT NOT NULL, INDEX IDX_B8F0BEA24329E6FB (directors_id), INDEX IDX_B8F0BEA28D7B4FB4 (tags_id), PRIMARY KEY(directors_id, tags_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE actors_tags ADD CONSTRAINT FK_6673CAE47168CF59 FOREIGN KEY (actors_id) REFERENCES actors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actors_tags ADD CONSTRAINT FK_6673CAE48D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE directors_tags ADD CONSTRAINT FK_B8F0BEA24329E6FB FOREIGN KEY (directors_id) REFERENCES directors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE directors_tags ADD CONSTRAINT FK_B8F0BEA28D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE actors ADD image_file_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE directors ADD image_file_name VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE actors_tags');
        $this->addSql('DROP TABLE directors_tags');
        $this->addSql('ALTER TABLE actors DROP image_file_name');
        $this->addSql('ALTER TABLE directors DROP image_file_name');
    }
}
