<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519111634 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE films_actors DROP FOREIGN KEY FK_687F7B5C939610EE');
        $this->addSql('ALTER TABLE films_directors DROP FOREIGN KEY FK_D853BC81939610EE');
        $this->addSql('DROP TABLE films');
        $this->addSql('DROP TABLE films_actors');
        $this->addSql('DROP TABLE films_directors');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE films (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE films_actors (films_id INT NOT NULL, actors_id INT NOT NULL, INDEX IDX_687F7B5C939610EE (films_id), INDEX IDX_687F7B5C7168CF59 (actors_id), PRIMARY KEY(films_id, actors_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE films_directors (films_id INT NOT NULL, directors_id INT NOT NULL, INDEX IDX_D853BC81939610EE (films_id), INDEX IDX_D853BC814329E6FB (directors_id), PRIMARY KEY(films_id, directors_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE films_actors ADD CONSTRAINT FK_687F7B5C7168CF59 FOREIGN KEY (actors_id) REFERENCES actors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE films_actors ADD CONSTRAINT FK_687F7B5C939610EE FOREIGN KEY (films_id) REFERENCES films (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE films_directors ADD CONSTRAINT FK_D853BC814329E6FB FOREIGN KEY (directors_id) REFERENCES directors (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE films_directors ADD CONSTRAINT FK_D853BC81939610EE FOREIGN KEY (films_id) REFERENCES films (id) ON DELETE CASCADE');
    }
}
