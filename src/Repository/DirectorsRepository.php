<?php

namespace App\Repository;

use App\Entity\Directors;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Directors|null find($id, $lockMode = null, $lockVersion = null)
 * @method Directors|null findOneBy(array $criteria, array $orderBy = null)
 * @method Directors[]    findAll()
 * @method Directors[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DirectorsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Directors::class);
    }

    public function findByName($name)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.name = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Directors[] Returns an array of Directors objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Directors
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
