<?php

namespace App\Repository;

use App\Entity\ImageSlider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ImageSlider|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageSlider|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageSlider[]    findAll()
 * @method ImageSlider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageSliderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ImageSlider::class);
    }

    public function findByFilmId($id)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.filmId = :filmId')
            ->setParameter('filmId', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return ImageSlider[] Returns an array of ImageSlider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageSlider
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
