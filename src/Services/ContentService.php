<?php

namespace App\Services;

class ContentService
{
    public function formatBoxOffice($boxOffice)
    {
        if (strlen($boxOffice) == 9) {
            $boxOffice = str_split($boxOffice, 1);

            if ($boxOffice[3] == '0') {
                $number = [
                    $boxOffice[0],
                    $boxOffice[1],
                    $boxOffice[2]
                ];

                $boxOffice = implode('', $number) . ' millions';
            }
            else {
                $number = [
                    $boxOffice[0],
                    $boxOffice[1],
                    $boxOffice[2]
                ];

                $boxOffice = implode('', $number) . ',' . $boxOffice[3]  . ' millions';
            }
        }
        elseif (strlen($boxOffice) == 10) {
            $boxOffice = str_split($boxOffice, 1);
            
            $number = [
                $boxOffice[1],
                $boxOffice[2],
                $boxOffice[3]
            ];

            $boxOffice = $boxOffice[0] . ',' . implode('', $number) . ' milliards';
        }
        else {
            $boxOffice = $boxOffice;
        }

        return $boxOffice;
    }
    public function formatDuration($duration)
    {
        $duration = (float)$duration;
        
        if ($duration > 180) {
            $minutes = $duration - 180;

            if (strlen($minutes) == 2) {
                $minutes = $minutes;
            }
            else {
                $minutes = '0' . $minutes;
            }

            $duration = '3h' . $minutes . 'min';
        }
        elseif ($duration > 120) {
            $minutes = $duration - 120;

            if (strlen($minutes) == 2) {
                $minutes = $minutes;
            }
            else {
                $minutes = '0' . $minutes;
            }

            $duration = '2h' . $minutes . 'min';
        }
        elseif ($duration == 160) {
            $duration = '2h';
        }
        else {
            $minutes = $duration - 60;

            if (strlen($minutes) == 2) {
                $minutes = $minutes;
            }
            else {
                $minutes = '0' . $minutes;
            }

            $duration = '1h' . $minutes . 'min';
        }

        return $duration;
    }
}
