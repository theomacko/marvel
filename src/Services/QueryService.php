<?php

namespace App\Services;

class QueryService
{
    public function getSearchResults($request, $searchItem, $repository) {
        if ($searchItem != 'vide') {
            $searchItem = strtolower($searchItem);
            $type = $request->request->get('type');
            
            $results = $repository->findAll();
            $elementType = [];

            foreach ($results as $result) {
                if ($type == 'film') {
                    $resultCompare = $result->getTitle();
                } else {
                    $resultCompare = $result->getName();
                }

                $resultCompare = strtolower($resultCompare);
                $itemSearched = explode(' ', $searchItem);

                if (count($itemSearched) > 1) {
                    $allWords = explode(' ', $resultCompare);

                    $n = 0;

                    foreach ($itemSearched as $item) {
                        if (in_array($item, $allWords)) {
                            $n ++;
                        }
                    }

                    if ($n == count($itemSearched)) {
                        array_push($elementType, $result);
                    }
                }
                else {
                    if (strpos($resultCompare, $searchItem) !== false) {
                        array_push($elementType, $result);
                    }
                }
            }

            $searchResult = count($elementType);
            
            return [
                'result' => $elementType,
                'searchItem' => $searchItem,
                'searchResult' => $searchResult
            ];
        }
        else {
            $results = $repository->findAll();

            return [
                'result' => $results,
                'searchItem' => null,
                'searchResult' => null
            ];
        }
    }
    public function getRelatedFilms($film, $allMovies) {
        $filmTags = $film->getMovieTag()->toArray();

        $listMovies = [];
        $relatedFilms = [];

        foreach ($allMovies as $movie) {
            if ($movie->getTitle() != $film->getTitle()) {
                array_push($listMovies, $movie);
            }
        }   
        
        foreach ($listMovies as $sameTagMovie) {
            $listTags = $sameTagMovie->getMovieTag()->toArray();
            $inList = 0;

            if (count($filmTags) == 1) {
                if (count($listTags) == 1) {
                    $firstTag = array_shift($filmTags);

                    if (in_array($firstTag, $listTags)) {
                        $inList ++;
                    }
    
                    array_unshift($filmTags, $firstTag);
                    
                    if ($inList == 1) {
                        array_push($relatedFilms, $sameTagMovie);
                    }
                }
            } 
            else if (count($filmTags) > 1) {
                foreach ($filmTags as $tag) {
                    if (in_array($tag, $listTags)) {
                        $inList ++;
                    }
                }

                if ($inList > 1) {
                    array_push($relatedFilms, $sameTagMovie);
                }
            }
        }

        return $relatedFilms;
    }
}
