<?php

namespace App\Twig;

use Twig\TwigFunction;
use App\Services\ContentService;
use Twig\Extension\AbstractExtension;

class TwigExtensions extends AbstractExtension
{
    private $contentService;

    public function __construct(ContentService $contentService)
    {
        $this->contentService = $contentService;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('get_formatted_box_office', [$this, 'getFormattedBoxOffice']),
            new TwigFunction('get_formatted_duration', [$this, 'getFormattedDuration']),
        ];
    }

    public function getFormattedBoxOffice($boxOffice)
    {
        return $this->contentService->formatBoxOffice($boxOffice);
    }

    public function getFormattedDuration($duration)
    {
        return $this->contentService->formatDuration($duration); 
    }
}
